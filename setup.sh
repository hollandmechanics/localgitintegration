#!/bin/bash

# run this from $NOTIFY_DIR

set -e

origin=$1
mkdir `basename $origin | sed 's/\.git$//' `
cd `basename $origin | sed 's/\.git$//' `
git init
git remote add origin $origin
git config credential.helper store
git checkout -b credential-updates || git checkout credential-updates
git commit --allow-empty -m 'Allow automatic notifications'
git push -u origin credential-updates
