# what

To integrate local git server with microsoft chat, across bitbucket, without
pushing actual code to the cloud.

Bitbucket's cloud integration will mostly take care of the chat notifications.
The post-receive hook will push commit messages and branch names to bitbucket.

# howto

- set up a bitbucket repo that can source the commit notifications
- set up the bitbucket connector in the chat, so that commits will appear there
- install the post-receive hook on the local git repo that should publish the
  commit messages to the bitbucket repo
 - make sure `$NOTIF_DIR`, `$PROJECT`, and `$LOCAL_REPO` are configured
   correctly
- run `./setup.sh <A_BITBUCKET_REPO_URL>` from within `$NOTIF_DIR`, this will
  store the repo's password so that it can commit automatically.


